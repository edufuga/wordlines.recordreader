package com.gitlab.edufuga.wordlines.recordreader;

import com.gitlab.edufuga.wordlines.core.WordStatusDate;

import java.io.IOException;
import java.text.ParseException;

public interface RecordReader {
    WordStatusDate readRecord(String word) throws IOException, ParseException;
    WordStatusDate readRecord(String word, boolean cached) throws IOException, ParseException;

    // FIXME: This is actually quite UGLY. The cache shouldn't belong to the RecordReader!
    // The cache is refreshed also by the RecordWriter (the filesystem implementation of it).
    // This has to be solved in a BETTER way!
    // A possible idea would be simply to extract the cache. Another idea would be to have two different caches in the
    // Reader and the Writer. In any case the current implementation is simply BAD. This method must be deleted!
    void updateCache(String word, WordStatusDate record);
}
