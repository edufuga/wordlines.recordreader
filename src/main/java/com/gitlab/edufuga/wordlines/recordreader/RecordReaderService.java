package com.gitlab.edufuga.wordlines.recordreader;

import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class RecordReaderService {
    private final Path statusRoot;
    private final Map<String, RecordReader> readersByLanguage;

    public RecordReaderService() {
        this.readersByLanguage = new HashMap<>();

        String vocabularyRootAsString = System.getProperty("bookwards.vocabulary");
        if (vocabularyRootAsString == null || vocabularyRootAsString.isEmpty()) {
            Map<String, String> env = System.getenv();
            if (!env.containsKey("WORDSFILES")) {
                throw new RuntimeException("Neither the system property 'bookwards.vocabulary' " +
                        "nor the environment variable 'WORDSFILES' was set! You have to tell me where your vocabulary " +
                        "is found.");
            }
            vocabularyRootAsString = env.get("WORDSFILES");
        }
        System.out.println("Vocabulary root: " + vocabularyRootAsString);

        this.statusRoot = Paths.get(vocabularyRootAsString).resolve("status");
        System.out.println(statusRoot.toAbsolutePath());
        if (Files.notExists(statusRoot)) {
            throw new RuntimeException("Words files root not found");
        }
    }

    public String readRecord(String language, String word) throws Exception {
        if (language == null || language.isEmpty()) {
            return "No language";
        }
        language = language.toUpperCase();

        if (word == null || word.isEmpty()) {
            return "No word";
        }

        RecordReader recordReader = getRecordReaderFor(language);

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();

        String result = "";

        try {
            WordStatusDate record = recordReader.readRecord(word);
            if (record.getStatus() == null) {
                result = "Word '"+word+"' not present.";
            }
            else {
                result = gson.toJson(record);
            }
        }
        catch (Exception ignored) {
            result = "Word not present?";
        }

        return result + System.lineSeparator();
    }

    public Set<String> getWords(String language) {
        if (language == null || language.isEmpty()) {
            return Collections.emptySet();
        }

        if (statusRoot == null) {
            return Collections.emptySet();
        }
        language = language.toUpperCase();

        Path statusFolder = statusRoot.resolve(language);

        if (!Files.isDirectory(statusFolder)) {
            return Collections.emptySet();
        }

        Set<String> fileNames;
        try {
            Supplier<TreeSet<String>> words = () -> new TreeSet<>(Comparator.naturalOrder());

            fileNames = Files.list(statusFolder)
                    .filter(Files::isReadable)
                    .map(f -> f.getFileName().toString())
                    .map(n -> n.replace(".tsv", ""))
                    .collect(Collectors.toCollection(words));
        } catch (IOException e) {
            e.printStackTrace();
            fileNames = Collections.emptySet();
        }

        return fileNames;
    }

    public List<WordStatusDate> getRecords(String language) {
        if (language == null || language.isEmpty()) {
            return Collections.emptyList();
        }

        if (statusRoot == null) {
            return Collections.emptyList();
        }
        language = language.toUpperCase();

        Path statusFolder = statusRoot.resolve(language);

        if (!Files.isDirectory(statusFolder)) {
            return Collections.emptyList();
        }

        RecordReader recordReader;
        try {
            recordReader = getRecordReaderFor(language);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }

        List<WordStatusDate> records;
        try {
            records = Files.list(statusFolder)
                    .filter(Files::isReadable)
                    .map(f -> {
                        try {
                            return recordReader.readRecord(f.getFileName().toString().replace(".tsv", ""));
                        } catch (IOException | ParseException e) {
                            e.printStackTrace();
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            records = Collections.emptyList();
        }

        return records;
    }

    private RecordReader getRecordReaderFor(String language) throws Exception {
        Path statusFolder = statusRoot.resolve(language);
        readersByLanguage.putIfAbsent(language, new FileSystemRecordReader(statusFolder.toAbsolutePath().toString(), null));
        return readersByLanguage.get(language);
    }

    public static void main(String[] args) {
        System.out.println("The RecordReaderService is meant to be used as a dependency.");
    }
}
