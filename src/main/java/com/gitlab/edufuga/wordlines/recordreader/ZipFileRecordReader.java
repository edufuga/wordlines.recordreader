package com.gitlab.edufuga.wordlines.recordreader;

import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import java.io.IOException;
import java.nio.file.*;
import java.text.ParseException;
import java.util.*;

public class ZipFileRecordReader implements RecordReader {
    private final Map<String, WordStatusDate> recordCache = new HashMap<>();
    private final Path nativeRoot;
    private static FileSystem zipFS = FileSystems.getDefault();

    private Path root;
    private static FileSystem fs = FileSystems.getDefault();

    private boolean hasInMemoryFileSystem = false;

    public ZipFileRecordReader(String statusZipFile, Path sentencesFolder) throws Exception {
        nativeRoot = Paths.get(statusZipFile);
        System.out.println (nativeRoot);

        if (Files.isDirectory(nativeRoot)) {
            throw new RuntimeException("For the moment only ZIP files are accepted!");
        }
        else {
            zipFS = FileSystems.newFileSystem(nativeRoot, (ClassLoader) null);
        }

        Path concreteTSVFile = zipFS.getPath("huis.tsv");
        System.out.println("zipPath = " + concreteTSVFile.toAbsolutePath());
        System.out.println(Files.exists(concreteTSVFile));
        List<String> lines = Files.readAllLines(concreteTSVFile);
        lines.forEach(System.out::println);

        if (sentencesFolder != null) {
            loadInMemory(sentencesFolder);
        }
    }

    public ZipFileRecordReader(String zip) throws Exception {
        this(zip, null);
    }

    private void loadInMemory(Path sentencesFolder) throws Exception {
        fs = Jimfs.newFileSystem(Configuration.unix());
        root = fs.getPath("");
        boolean ok = Files.exists(root);

        // Load words from the metadata.tsv file in the sentences folder (contains the words and their frequency).
        Path metadata = sentencesFolder.resolve("metadata.tsv");
        if (Files.notExists(metadata)) {
            throw new Exception("Metadata file containing words and their frequency does not exist in " + String.valueOf(sentencesFolder) + ".");
        }


        List<String> words = new ArrayList<>();

        List<String> lines = Files.readAllLines(metadata);
        lines.forEach((String line) -> {
            String[] strings = line.split("\t");
            String word = strings[0];
            words.add(word);
        });

        System.out.println("Number of words in sentences folder: " + words.size());

        // TODO: Comprovar si funciona bé (segurament no). Què passa quan es *guarden* arxius nous mentre s'executa?
        for (String word : words) {
            copyToInMemoryFileSystem(word);
        }

        hasInMemoryFileSystem = true;
    }

    private void copyToInMemoryFileSystem(String word) throws IOException {
        String filename = word + ".tsv";
        Path wordFile = zipFS.getPath(filename);

        // Nota: Aquí no es consideren majúscules i minúscules, sinó que s'assumeix que les paraules tenen arxius
        // corresponents amb exactament la mateixa paraula com a nom d'arxiu. No es 'corregeix' ni es busca diverses
        // vegades fins a trobar l'arxiu correcte.

        if (Files.exists(wordFile)) {
            System.out.println ("Copying " + wordFile + " into memory.");
            Files.copy(wordFile, root.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @Override
    public WordStatusDate readRecord(String word, boolean cached) throws IOException, ParseException {
        if (cached) {
            if (recordCache.containsKey(word)) {
                return recordCache.get(word);
            }

        } else {
            if (hasInMemoryFileSystem) {
                // Refresh (reload) the file definition from the hard drive (disk) into the memory filesystem.
                String filename = word + ".tsv";
                Path wordFile = nativeRoot.resolve(filename);
                Files.copy(wordFile, root.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
            }
        }


        if (hasInMemoryFileSystem) {
            // FIXME: This is NOT working right now! (I didn't check it, but it WON'T work. ZIP VS./PLUS JimFS.

            // The idea here is to extend this from (not only?) using the native folder structure but (also?) ZIP and/or an
            // in-memory filesystem (JimFS) in order to increase the performance (caching or loading everything on start).
            // This is not a trivial change because it means handling the synchronization (saving on disk) the changes at some
            // point in time (every N changes and/or at shutdown of the VM [is there a VM on Android?]).
            Path root = fs.getPath("");
            WordStatusDate record = resolveWord(word);
            if (record.getStatus() == null) {
                // The record was not present in memory, so we try to load it from disk.
                // FIXME: This doesn't take care of OVERRIDING changes, only of new files.
                record = resolveWord(word);
            }

            return record;
        } else {
            WordStatusDate record = resolveWord(word);
            return record;
        }
    }

    @Override
    public WordStatusDate readRecord(String word) throws IOException, ParseException {
        return readRecord(word, true);
    }

    private WordStatusDate resolveWord(String word) throws ParseException, IOException {
        String wordInFileSystem = word;

        Path wordFile = zipFS.getPath(word + ".tsv");
        if (Files.notExists(wordFile)) {
            System.out.println (wordFile + " not exists");
            // Try with lowercase *file*. The record should contain the normal word (no lowercase).
            wordInFileSystem = word.toLowerCase();
            wordFile = zipFS.getPath(wordInFileSystem + ".tsv");
            if (Files.notExists(wordFile)) {
                // Try with capitalized file. This transforms for example "NICOLAS" to "Nicolas". This is a very rare use case.
                wordInFileSystem = capitalize(word.toLowerCase());
                wordFile = root.resolve(wordInFileSystem + ".tsv");
                if (Files.notExists(wordFile)) {
                    // Mark missing status with null in the attribute "status" (TODO: Improve?)
                    return new WordStatusDate(word, null, new Date());
                } else {
                    // The file reference was redefined, so that the following code should work properly...
                }
            }
        }

        String recordContent = first(Files.readAllLines(wordFile));
        String[] strings = recordContent.split("\t");
        String realWord = strings[0]; // Take the word from the file contents (the original word could have wrong case)
        String status = strings[1];
        String date = strings[2];
        WordStatusDate record = new WordStatusDate(realWord, status, date);

        updateCache(wordInFileSystem, record);

        return record;
    }

    // This is called by the RecordWriter. The method is in this class because the RecordReader has the cache inside.
    // This is not really cleanly designed, but that could be said about the whole code, which is quite prototypical.
    public void updateCache(WordStatusDate record) {
        recordCache.put (record.getWord(), record);

        // In case there is an in-memory filesystem, it should also be kept in sync. The new record must be copied there.
        if (hasInMemoryFileSystem) {
            try {
                // This does NOT consider the case that the file in the filesystem has different case than the word.
                copyToInMemoryFileSystem(record.getWord());
            }
            catch (IOException ignored) {

            }
        }
    }

    @Override
    public void updateCache(String wordInFileSystem, WordStatusDate record) {
        recordCache.put (record.getWord(), record);

        // In case there is an in-memory filesystem, it should also be kept in sync. The new record must be copied there.
        if (hasInMemoryFileSystem) {
            try {
                copyToInMemoryFileSystem(wordInFileSystem);
            }
            catch (IOException ignored) {

            }
        }
    }

    public List<WordStatusDate> readRecords(List<String> words) throws IOException, ParseException {
        // Read records from the list of words
        // N.B: Words without a record will be ignored. The list only contains known words (i.e. that have a record)
        List<WordStatusDate> records = new ArrayList<>();
        for (String word : words) {
            WordStatusDate record = readRecord(word);
            records.add(record);
        }

        return records;
    }

    private static String capitalize(CharSequence self) {
        return self.length() == 0 ? "" : "" + Character.toUpperCase(self.charAt(0)) + self.subSequence(1, self.length());
    }

    private static <T> T first(List<T> self) {
        if (self.isEmpty()) {
            throw new NoSuchElementException("Cannot access first() element from an empty List");
        }
        return self.get(0);
    }

    // Example arguments:
    // /media/eduard/dades/Baixades/WordsFiles/src/main/resources/status/NL.zip "/media/eduard/dades/Baixades/EBooks/NL/Harry Potter en de steen der wijzen - Joanne Rowling/book" huis
    public static void main(String[] args) throws Exception {
        String statusZipFile = args[0];
        String sentencesFolder = args[1];
        String word = args[2];

        RecordReader recordReader = new ZipFileRecordReader(statusZipFile, Paths.get(sentencesFolder));
        WordStatusDate record = recordReader.readRecord(word);
        System.out.println(record);
    }
}
