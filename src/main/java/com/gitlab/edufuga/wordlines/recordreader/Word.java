package com.gitlab.edufuga.wordlines.recordreader;

import java.util.Objects;

public class Word implements Comparable<Word> {
    private final String word;
    private final String url;

    public Word(String word, String url) {
        this.word = word;
        this.url = url;
    }

    public String getWord() {
        return word;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return Objects.equals(word, word1.word) && Objects.equals(url, word1.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, url);
    }

    @Override
    public String toString() {
        return "Word{" +
                "word='" + word + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public int compareTo(Word that) {
        return this.word.compareTo(that.word);
    }
}
